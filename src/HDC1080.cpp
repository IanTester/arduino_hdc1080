/*
HDC1080.cpp - HDC1080 class
Copyright (C) 2016 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <Arduino.h>
#include <Wire.h>
#include "HDC1080.h"

uint16_t HDC1080::_read_reg(Register reg) {
  _I2C_error = false;
  Wire.beginTransmission(HDC1080_ADDRESS);
  size_t written = Wire.write(static_cast<uint8_t>(reg));
  Wire.endTransmission();
  if (written == 0) {
    _I2C_error = true;
    return 0;
  }

  delay(7);

  size_t read = Wire.requestFrom(HDC1080_ADDRESS, 2);
  if (read < 2) {
    _I2C_error = true;
    return 0;
  }

  int d = Wire.read();
  if (d == -1) {
    _I2C_error = true;
    return 0;
  }
  uint16_t ret = (uint8_t)d << 8;

  d = Wire.read();
  if (d == -1) {
    _I2C_error = true;
    return 0;
  }
  ret |= d;

  Wire.endTransmission();

  return ret;
}

void HDC1080::_write_reg(Register reg, uint16_t val) {
  _I2C_error = false;
  Wire.beginTransmission(HDC1080_ADDRESS);

  size_t written = Wire.write(static_cast<uint8_t>(reg));
  if (written == 0) {
    _I2C_error = true;
    return;
  }

  written = Wire.write((uint8_t)(val >> 8));
  if (written == 0) {
    _I2C_error = true;
    return;
  }

  written = Wire.write((uint8_t)(val & 0xff));
  if (written == 0) {
    _I2C_error = true;
    return;
  }

  Wire.endTransmission();
}

unsigned long HDC1080::_t_conv_time(uint16_t config) const {
  return config & enum_val(Configuration_mask::TRES) ? 7 : 4;
}

unsigned long HDC1080::_h_conv_time(uint16_t config) const {
  switch (config & enum_val(Configuration_mask::HRES)) {
  case enum_val(HRES::_14bit):
    return 7;
  case enum_val(HRES::_11bit):
    return 4;
  default:
    break;
  }
  return 3;
}

HDC1080::HDC1080() :
  _I2C_error(false),
  _next_t(false),
  _next_h(false)
{}

void HDC1080::begin(void) {
  _I2C_error = false;
  _next_t = _next_h = false;
}

bool HDC1080::I2C_error(void) const {
  return _I2C_error;
}

void HDC1080::serial_id(uint8_t buffer[5]) {
  uint16_t a, b, c;
  a = _read_reg(Register::SerialID0);
  b = _read_reg(Register::SerialID1);
  c = _read_reg(Register::SerialID2);

  buffer[0] = c >> 8;
  buffer[1] = b & 0xff;
  buffer[2] = b >> 8;
  buffer[3] = a & 0xff;
  buffer[4] = a >> 8;
}

uint16_t HDC1080::manufacturer_id(void) {
  return _read_reg(Register::ManufacturerID);
}

uint16_t HDC1080::device_id(void) {
  return _read_reg(Register::DeviceID);
}

void HDC1080::set_hres(uint8_t res) {
  HRES bits;
  switch (res) {
  case 14:
    _modify_config(Configuration_mask::HRES, HRES::_14bit);
    break;
  case 11:
    _modify_config(Configuration_mask::HRES, HRES::_11bit);
    break;
  case 8:
  default:
    _modify_config(Configuration_mask::HRES, HRES::_8bit);
    break;
  }
}

void HDC1080::set_tres(uint8_t res) {
  TRES bits;
  switch (res) {
  case 14:
    _modify_config(Configuration_mask::TRES, TRES::_14bit);
    break;
  case 11:
  default:
    _modify_config(Configuration_mask::TRES, TRES::_11bit);
    break;
  }
}

bool HDC1080::battery_low(void) {
  return _read_reg(Register::Configuration) & enum_val(Configuration_mask::BTST);
}

bool HDC1080::heater(void) {
  return _read_reg(Register::Configuration) & enum_val(Configuration_mask::HEAT);
}

void HDC1080::set_heater(bool on) {
  _modify_config(Configuration_mask::HEAT, on ? HEAT::On : HEAT::Off);
}

void HDC1080::reset(void) {
  _modify_config(Configuration_mask::RST, RST::Software);
  _next_t = _next_h = false;
}

unsigned long HDC1080::measure_temperature(void) {
  _modify_config(Configuration_mask::MODE, MODE::Seperate);

  _write_reg(Register::Temperature, 0);

  _next_t = true;
  _next_h = false;
  return _t_conv_time(_read_reg(Register::Configuration));
}

unsigned long HDC1080::measure_humidity(void) {
  _modify_config(Configuration_mask::MODE, MODE::Seperate);

  _write_reg(Register::Humidity, 0);

  _next_t = false;
  _next_h = true;
  return _h_conv_time(_read_reg(Register::Configuration));
}

unsigned long HDC1080::measure_both(void) {
  _modify_config(Configuration_mask::MODE, MODE::Both);

  _write_reg(Register::Temperature, 0);
  uint16_t config = _read_reg(Register::Configuration);

  _next_t = _next_h = true;
  return _t_conv_time(config) + _h_conv_time(config);
}

HDC1080_reading* HDC1080::get_reading(void) {
  auto reading = new HDC1080_reading;
  if (_next_t)
    reading->_set_rt(_read_reg(Register::Temperature));
  if (_next_h)
    reading->_set_rh(_read_reg(Register::Humidity));

  _next_t = _next_h = false;
  return reading;
}


HDC1080_reading::HDC1080_reading(void) :
  _rt(0), _rh(0),
  _have_t(false), _have_h(false)
{}

void HDC1080_reading::_set_rt(uint16_t rt) {
  _rt = rt;
  _have_t = true;
}

void HDC1080_reading::_set_rh(uint16_t rh) {
  _rh = rh;
  _have_h = true;
}

bool HDC1080_reading::haveT(void) const {
  return _have_t;
}

bool HDC1080_reading::haveH(void) const {
  return _have_h;
}

float HDC1080_reading::temperature(void) const {
  return ((float)_rt * 0.0025177f) - 40.0;
}

float HDC1080_reading::humidity(void) const {
  return (float)_rh * 0.00152588f;
}
