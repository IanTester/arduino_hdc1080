/*
HDC1080.h - HDC1080 class
Copyright (C) 2016 Ian Tester

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#pragma once

#include <stdint.h>

#define HDC1080_ADDRESS 0x40

template <typename R>
constexpr uint16_t enum_val(const R reg) { return static_cast<uint16_t>(reg); }

class HDC1080_reading;

class HDC1080 {
private:
  enum class Register : uint8_t {
    Temperature,
    Humidity,
    Configuration,

    SerialID0		= 0xfb,
    SerialID1,
    SerialID2,
    ManufacturerID,
    DeviceID,
  };

  enum class Configuration_mask : uint16_t {
    // 8 LSB are reserved

    HRES		= 0x0300,
    TRES		= 0x0400,
    BTST		= 0x0800,
    MODE		= 0x1000,
    HEAT		= 0x2000,
    // reserved
    RST			= 0x8000,
  };

  enum class HRES : uint16_t {
    _14bit		= 0x0000,
    _11bit		= 0x0100,
    _8bit		= 0x0200,
  };

  enum class TRES : uint16_t {
    _14bit		= 0x0000,
    _11bit		= 0x0400,
  };

  enum class BTST : uint16_t {
    Good		= 0x0000,
    Bad			= 0x0800,
  };

  enum class MODE : uint16_t {
    Both		= 0x0000,
    Seperate		= 0x1000,
  };

  enum class HEAT : uint16_t {
    Off			= 0x0000,
    On			= 0x2000,
  };

  enum class RST : uint16_t {
    Normal		= 0x0000,
    Software		= 0x8000,
  };

  bool _I2C_error;
  bool _next_t, _next_h;

  uint16_t _read_reg(Register reg);
  void _write_reg(Register reg, uint16_t val);

  unsigned long _t_conv_time(uint16_t config) const;
  unsigned long _h_conv_time(uint16_t config) const;

  template <typename S>
  uint16_t _modify_config(Configuration_mask mask, S set) {
    uint16_t config = _read_reg(Register::Configuration);
    uint16_t new_config = (config & ~enum_val(mask)) | enum_val(set);
    if (new_config != config) {
      _write_reg(Register::Configuration, new_config);
      config = new_config;
    }
    return config;
  }


public:
  HDC1080();

  void begin(void);

  bool I2C_error(void) const;

  void serial_id(uint8_t buffer[5]);
  uint16_t manufacturer_id(void);
  uint16_t device_id(void);

  void set_tres(uint8_t res);
  void set_hres(uint8_t res);

  bool battery_low(void);

  bool heater(void);
  void set_heater(bool on = true);

  void reset(void);

  unsigned long measure_temperature(void);
  unsigned long measure_humidity(void);
  unsigned long measure_both(void);

  HDC1080_reading* get_reading(void);

}; // class HDC1080


class HDC1080_reading {
private:
  uint16_t _rt, _rh;
  bool _have_t, _have_h;

  friend class HDC1080;

  HDC1080_reading(void);

  void _set_rt(uint16_t rt);

  void _set_rh(uint16_t rh);


public:
  bool haveT(void) const;
  bool haveH(void) const;

  float temperature(void) const;
  float humidity(void) const;

}; // class HDC1080_reading
