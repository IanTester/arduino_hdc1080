#include <Wire.h>
#include <HDC1080.h>

HDC1080 hdc;

void setup(void) {
  Serial.begin(115200);
  Serial.println();
  Serial.println(__TIMESTAMP__);
  Serial.println("HDC1080 test sketch.");

  // Setup I2C (on ESP8266)
  Wire.begin(13, 4);
  Wire.setClock(100000);

  delay(15);
  hdc.begin();
  hdc.set_tres(14);
  hdc.set_hres(14);
}

void loop(void) {
  unsigned long time = hdc.measure_both();
  delay(time);

  auto r = hdc.get_reading();
  if (r != nullptr) {
    if (r->haveT()) {
      Serial.print(r->temperature(), 2);
      Serial.print(" °C\t");
    }
    if (r->haveH()) {
      Serial.print(r->humidity(), 2);
      Serial.println(" %RH");
    }
  }

  delay(2000);
}
